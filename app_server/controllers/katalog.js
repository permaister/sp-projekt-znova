var request = require('request');
var apiParametri = {
  streznik: "http://localhost:" + process.env.PORT
};
if (process.env.NODE_ENV === 'production') {
  apiParametri.streznik = "https://spletnik.herokuapp.com";
}


/* Prikaži osnovno stran - katalog izdelkov */
module.exports.seznam = function(req, res) {
      prikaziZacetniSeznam(req, res);
};

var prikaziZacetniSeznam = function(zahteva, odgovor){
  odgovor.render('seznam', { 
    title: 'Spletnik',
    glava: 'Poiščite vaše najljubše knjige in filme po ugodnih cenah',
  });
};


/* Prikaži podrobnosti o izdelku */
module.exports.izdelek = function(req, res) {
  pridobiPodrobnostiIzdelka(req, res, function(zahteva, odgovor, vsebina){
    prikaziPodrobnostiIzdelka(zahteva, odgovor, vsebina);
  });
};

var prikaziPodrobnostiIzdelka = function(zahteva, odgovor, izdelek){
  var brezKomentarja= "";
  if(!izdelek.komentarji.length){
    brezKomentarja = "Ta artikel še nima komentarjev. Bodi prvi in dodaj svoj komentar.";
  }
  odgovor.render('izdelek', { 
    title: izdelek.naziv ,
    izdelek: izdelek,
    brezKomentarja: brezKomentarja
  });
};

var prikaziNapako = function(zahteva, odgovor, status){
  var naslov, vsebina;
  if(status == 404){
    naslov = "404, Vsebine ni mogole najti."
    vsebina = "Očitno je nekje prišlo do napake. Pridite pogledat pozneje, če je napaka odpravljena ali pojdite na drug stran."
  }
  else{
    naslov = status+" Ups, to je pa nerodno. Nekaj je šlo narobe."
    vsebina = "Žal je prišlo do napake. Lahko poskusite osvežiti stran, ali pa poiskati kaj drugega."
  }
  odgovor.status(status);
  odgovor.render('generik', {
    title : naslov,
    vsebina : vsebina
  })
}

var pridobiPodrobnostiIzdelka = function(req, res, callback){
  var parametriZahteve, pot;
  pot = '/api/izdelki/' + req.params.idIzdelka;
  parametriZahteve = {
    url: apiParametri.streznik + pot,
    method: 'GET',
    json: {}
  };
  request(
    parametriZahteve, 
    function(napaka, odgovor, vsebina){
      if(odgovor.statusCode == 200){
        callback(req, res, vsebina);
      }
      else{
        prikaziNapako(req, res, odgovor.statusCode);
      }
  });
};


/* Prikaži dodajanje komentarja */
module.exports.dodajKomentar = function(req, res) {
  console.log("Prikazujem pogled za dodajanje komentarja.");
  pridobiPodrobnostiIzdelka(req, res, function(zahteva, odgovor, vsebina){
    prikaziObrazecZaDodajanjeKomentarja(req, res, vsebina);
  });
};


var prikaziObrazecZaDodajanjeKomentarja = function(zahteva, odgovor, izdelek){
  console.log("Prikazujem pogled za dodajanje komentarja. V metodi");
  odgovor.render('dodajKomentar', { 
    title: 'Dodaj komentar',
    naziv: izdelek.naziv,
    napaka: zahteva.query.napaka,
    url: zahteva.originalUrl
  });
};

/* Shranjevanje novega komentarja */
module.exports.shraniKomentar = function(req, res) {
  console.log("Zacetek metode shranjevanje komentarja.");
  var parametriZahteve, pot, idIzdelka, posredovaniPodatki;
  idIzdelka = req.params.idIzdelka;
  pot = '/api/izdelki/' + idIzdelka + '/komentarji';
  posredovaniPodatki = {
    naziv: req.body.naziv,
    ocena: req.body.ocena,
    komentar: req.body.komentar
  };
  parametriZahteve = {
    url: apiParametri.streznik + pot,
    method: 'POST',
    json: posredovaniPodatki
  };
  if (!posredovaniPodatki.naziv || !posredovaniPodatki.ocena || !posredovaniPodatki.komentar) {
    res.redirect('/izdelek/' + idIzdelka + '/komentar/nov?napaka=vrednost');
  } else {
    request(parametriZahteve, function(napaka, odgovor, vsebina){
      if(odgovor.statusCode == 201){
        res.redirect('/izdelek/'+idIzdelka);
      }else if(odgovor.statusCode === 400 && vsebina.name && vsebina.name === "ValidationError"){
        res.redirect('/izdelek/' + idIzdelka + '/komentar/nov?napaka=vrednost');
      }else{
        prikaziNapako(req, res, odgovor.statusCode);
      }
    });
  }
};

/* Prikaži napredno iskanje */
module.exports.naprednoIskanje = function(req, res) {
  res.render('index', { title: 'Napredno iskanje' });
};

/* Prikaži administratorsko nadzorno ploščo */
module.exports.admin = function(req, res) {
  res.render('index', { title: 'Administratorska nadzorna plošča' });
};