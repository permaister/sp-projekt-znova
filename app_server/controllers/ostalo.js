/* Prikaži informacije o podjetju */
module.exports.informacije = function(req, res) {
  res.render('informacije', { title: 'Informacije o strani' });
};

/* Vrni stran z Angular SPA */
module.exports.angularApp = function(req, res) {
    res.render('layout', {
        title: 'Spletnik',
        glava: 'Poiščite vaše najljubše knjige in filme po ugodnih cenah'
    });
};