var express = require('express');
var router = express.Router();
var ctrlKatalog = require('../controllers/katalog');
var ctrlOstalo = require('../controllers/ostalo');

/* Strani v povezavi s katalogom */
router.get('/', ctrlOstalo.angularApp);
router.get('/izdelek/:idIzdelka', ctrlKatalog.izdelek);
router.get('/izdelek/:idIzdelka/komentar/nov', ctrlKatalog.dodajKomentar);
router.post('/izdelek/:idIzdelka/komentar/nov', ctrlKatalog.shraniKomentar);
router.get('/napredno', ctrlKatalog.naprednoIskanje);
router.get('/admin', ctrlKatalog.admin);



/* Ostale strani */
router.get('/informacije', ctrlOstalo.informacije);

module.exports = router;
