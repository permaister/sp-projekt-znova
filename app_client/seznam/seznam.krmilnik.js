(function() {
/* global angular */


seznamCtrl.$inject = ['$scope', 'trgovinaPodatki'];
function seznamCtrl($scope, trgovinaPodatki) {
    var vm = this;
    vm.glavaStrani = {
        title: 'Spletnik',
        podnaslov: 'Poiščite vaše najljubše knjige in filme po ugodnih cenah',
    };
    
    vm.sporocilo = "Pridobivam izdelke iz kataloga.";
    
    vm.pridobiPodatke = function(){
        trgovinaPodatki.seznam().then(
            function success(odgovor){
                console.log("V success");
                vm.data = {izdelki : odgovor.data};
                vm.sporocilo = "";
                console.log(vm.data);
            },
            function error(odgovor){
                vm.sporocilo = "Prišlo je do napake!";
                console.log(odgovor.e);
            }
        );
    }

    trgovinaPodatki.seznam(vm.pridobiPodatke());
}

angular
  .module('spletna_trgovina')
  .controller('seznamCtrl', seznamCtrl);
  
})();