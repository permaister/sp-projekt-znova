(function() {
/* global angular */

var trgovinaPodatki = function($http, avtentikacija) {
      
      var seznam = function(){
          return $http.get('/api/izdelki');
      };
      
      var izdelek = function(idIzdelka){
          return $http.get('/api/izdelki/'+idIzdelka);
      };
      
      var dodajKomentar = function(idIzdelka, podatki){
         return $http.post('/api/izdelki/' + idIzdelka + '/komentarji', podatki, {
             headers: {
                 Authorization: 'Bearer ' + avtentikacija.vrniZeton()
             }
         }); 
      };
      
      var vrniKosarico = function(idUporabnika){
          console.log("Naslov: /api/kosarica/"+idUporabnika);
          return $http.get('/api/kosarica/'+idUporabnika, {
              headers: {
                 Authorization: 'Bearer ' + avtentikacija.vrniZeton()
              }
          });
      }
      
      var dodajArtikel = function(podatki){
          console.log("Dodaj artikel v t.podatki klican!");
          console.log("Uporabnik: "+podatki.uporabnik);
          console.log("Artikel: "+podatki.idIzdelka);
          return $http.post('/api/kosarica/'+podatki.uporabnik, podatki, {
              headers: {
                  Authorization: 'Bearer ' + avtentikacija.vrniZeton()
              }
          });
      }
      
      var odstraniArtikel = function(podatki){
          console.log('/api/kosarica/'+podatki.uporabnik);
          console.log("Zeton: "+avtentikacija.vrniZeton());
          return $http.post('/api/kos/'+podatki.uporabnik, podatki, {
              headers: {
                  Authorization: 'Bearer ' + avtentikacija.vrniZeton(),
                  idIzdelka : podatki.idIzdelka
              }
          });
      }
      
      var zakljuciNakup = function(idUporabnika, podatki){
        console.log("id uporabnika: "+idUporabnika);
        return $http.post('/api/zak/'+idUporabnika, {
              headers: {
                  Authorization: 'Bearer ' + avtentikacija.vrniZeton()
              }
          });
      }
      
      return {
          seznam : seznam,
          izdelek : izdelek,
          dodajKomentar : dodajKomentar,
          vrniKosarico : vrniKosarico,
          dodajArtikel : dodajArtikel,
          odstraniArtikel : odstraniArtikel,
          zakljuciNakup : zakljuciNakup
      };
      
      
};

trgovinaPodatki.$inject = ['$http', 'avtentikacija'];

/*
var trgovinaPodatki = function($http) {
      var izdelki = function(){
          return $http.get('/api/izdelki');
      };
      return {
          izdelki : izdelki
      };
};
*/

angular
    .module('spletna_trgovina')
    .service('trgovinaPodatki', trgovinaPodatki);
    
})();