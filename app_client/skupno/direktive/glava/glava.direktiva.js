(function() {
  /* global angular */
  
  var glava = function() {
    return {
      restrict: 'EA',
      scope: {
        vsebina: '=glavaStrani'
      },
      templateUrl: "/skupno/direktive/glava/glava.predloga.html"
    };
  };
  
  angular
    .module('spletna_trgovina')
    .directive('glava', glava);
})();