(function() {

/* global angular */

var prikaziOceno = function() {
    return {
        restrict: 'EA',
        scope: {
            trenutnaOcena: '=ocena'
        },
        templateUrl: "/skupno/direktive/prikaziOceno/ocena-zvezdice.html"
    };
};

angular 
    .module('spletna_trgovina')
    .directive('prikaziOceno', prikaziOceno);
    
})();