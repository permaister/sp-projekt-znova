(function() {
  /* global angular */
  
  function kosaricaCtrl($routeParams, $location, $uibModal, trgovinaPodatki, avtentikacija) {
    var vm = this;
    
    vm.jePrijavljen = avtentikacija.jePrijavljen();
    vm.prvotnaStran = $location.path();
    vm.trenutniUporabnik = avtentikacija.trenutniUporabnik();
    
    console.log(vm.trenutniUporabnik.id);

    
    trgovinaPodatki.vrniKosarico(vm.trenutniUporabnik.id).then(
        function success(odgovor){
            console.log("Nasel kosarico!")
            vm.podatki = { 
                kosarica: odgovor.data
            };
            console.log("Tole je v kosarici");
            console.log(vm.podatki)
            console.log(vm.podatki.kosarica.artikli)
            console.log("To je trenutni uporabnik: "+vm.trenutniUporabnik.ime)
            vm.glavaStrani = {
                title: "Vasa kosarica"+ vm.trenutniUporabnik.ime
            };
        },
        function error(odgovor) {
            console.log("Ni ni, kosarice")
            console.log(odgovor.e);
        }
    );
    
    
    vm.odstraniArtikel = function(idIzdelka, $http){
        console.log("Odstranjujem artikel");
        console.log(vm.trenutniUporabnik);
        console.log("Tole je ID izdelka: "+idIzdelka);
        trgovinaPodatki.odstraniArtikel({
            uporabnik: vm.trenutniUporabnik.id,
            idIzdelka: idIzdelka,
        }).then(
            function success(odgovor){
                console.log("Success");
            },
            function error(odgovor){
                console.log("erorr!!");
                console.log(odgovor.e);
            }
            )
    }
    
    vm.zakljuciNakup = function(){
        trgovinaPodatki.zakljuciNakup(vm.trenutniUporabnik.id, {uporabnik: vm.trenutniUporabnik.id}).then(
            function success(odgovor){
                console.log("Success zak");
            },
            function error(odgovor){
                console.log("erorr zak!!");
                console.log(odgovor.e);
            })
        alert("Nakup zakljucen!");
    }
    

  };
  
  kosaricaCtrl.$inject = ['$routeParams', '$location', '$uibModal','trgovinaPodatki', 'avtentikacija', '$http'];
  
  angular
    .module('spletna_trgovina')
    .controller('kosaricaCtrl', kosaricaCtrl);
})();