(function() {
  /* global angular */
  
  function izdelekCtrl($routeParams, $location, $uibModal, trgovinaPodatki, avtentikacija) {
    var vm = this;
    
    vm.idIzdelka = $routeParams.idIzdelka;
    
    vm.jePrijavljen = avtentikacija.jePrijavljen();
    vm.trenutniUporabnik = avtentikacija.trenutniUporabnik();
    
    vm.prvotnaStran = $location.path();
    
    trgovinaPodatki.izdelek(vm.idIzdelka).then(
        function success(odgovor){
            var brez = "";
            if(odgovor.data.komentarji.length == 0){
                brez = "Ta artikel še nima komentarjev. Bodi prvi in dodaj svoj komentar."
            }
            vm.podatki = { 
                izdelki: odgovor.data,
                brezKomentarja: brez
            };
            console.log(vm.podatki)
            vm.glavaStrani = {
                title: vm.podatki.izdelki.naziv
            };
        },
        function error(odgovor) {
            console.log(odgovor.e);
        }
    );
    
    vm.prikaziPojavnoOknoObrazca = function() {
        var primerekModalnegaOkna = $uibModal.open({
            templateUrl: '/komentarModalnoOkno/komentarModalnoOkno.pogled.html',
            controller: 'komentarModalnoOkno',
            controllerAs: 'vm',
            resolve: {
                podrobnostiIzdelka: function(){
                    return{
                        idIzdelka: vm.idIzdelka,
                        nazivIzdelka: vm.podatki.izdelki.naziv
                    };
                }
            }
        });
        
        primerekModalnegaOkna.result.then(function(podatki) {
            if(typeof(podatki)!='undefined'){
                vm.podatki.izdelek.komentarji.push(podatki);
            }
        });
    };
    
    vm.dodajArtikel = function($http) {
        console.log("V metodi dodaj artikel");
        console.log("To je id artikla:"+vm.idIzdelka);
        console.log(vm.trenutniUporabnik.id);
        
        var izdelek = vm.idIzdelka;
        
        console.log("Uporabnik: "+vm.trenutniUporabnik.id+" Izdelek: "+izdelek);
        
        trgovinaPodatki.dodajArtikel({
            uporabnik: vm.trenutniUporabnik.id,
            idIzdelka: izdelek,
            kolicina : 1,
        }).then(
            function success(odgovor){
                console.log("Success");
            },
            function error(odgovor){
                console.log("erorr!!");
                console.log(odgovor.e);
            }
            )
    }

  };
  
  izdelekCtrl.$inject = ['$routeParams', '$location', '$uibModal','trgovinaPodatki', 'avtentikacija', '$http'];
  
  angular
    .module('spletna_trgovina')
    .controller('izdelekCtrl', izdelekCtrl);
})();