(function() {
  /* global angular */
  
  prijavaCtrl.$inject = ['$location', 'avtentikacija'];
  function prijavaCtrl($location, avtentikacija) {
    var vm = this;
    
    vm.glavaStrani = {
      naslov: 'Prijava v Spletnik'
    };
    
    vm.prijavniPodatki = {
      elektronskiNaslov: "",
      geslo: ""
    };
    
    vm.prvotnaStran = $location.search().stran || '/';
    
    vm.posiljanjePodatkov = function() {
      vm.napakaNaObrazcu = "";
      if (!vm.prijavniPodatki.elektronskiNaslov || !vm.prijavniPodatki.geslo) {
        vm.napakaNaObrazcu = "Zahtevani so vsi podatki, prosim poskusite znova!";
        return false;
      } else {
        vm.izvediPrijavo();
      }
    };
    
    vm.izvediPrijavo = function() {
      vm.napakaNaObrazcu = "";
      avtentikacija
        .prijava(vm.prijavniPodatki)
        .then(function success() {
          $location.search('stran', null);
          $location.path(vm.prvotnaStran);
        }, function error(napaka) {
          console.log(napaka);
          vm.napakaNaObrazcu = napaka.data.sporocilo;
        });
    };
  }
  
  angular
    .module('spletna_trgovina')
    .controller('prijavaCtrl', prijavaCtrl);
})();