(function() {
/* global angular */
angular.module('spletna_trgovina', ['ngRoute', 'ui.bootstrap']);

function nastavitev($routeProvider, $locationProvider) {
    console.log("Dosegel usmerjevalnik od Angularja.");
    $routeProvider
        .when('/', {
            templateUrl: 'seznam/seznam.pogled.html',
            controller: 'seznamCtrl',
            controllerAs: 'vm'
        })
        .when('/informacije', {
            templateUrl: 'informacije/informacije.pogled.html',
            controller: 'informacijeCtrl',
            controllerAs: 'vm'
        })
        .when('/izdelek/:idIzdelka', {
            templateUrl: 'izdelek/izdelek.pogled.html',
            controller: 'izdelekCtrl',
            controllerAs: 'vm'
        })
        .when('/registracija', {
            templateUrl: '/avtentikacija/registracija/registracija.pogled.html',
            controller: 'registracijaCtrl',
            controllerAs: 'vm'
        })
        .when('/prijava', {
            templateUrl: '/avtentikacija/prijava/prijava.pogled.html',
            controller: 'prijavaCtrl',
            controllerAs: 'vm'
        })
        .when('/kosarica', {
            templateUrl: '/kosarica/kosarica.pogled.html',
            controller: 'kosaricaCtrl',
            controllerAs: 'vm'
        })
        .otherwise({redirectTo: '/'});
        
    $locationProvider.html5Mode(true);
}

angular
    .module('spletna_trgovina')
    .config(['$routeProvider', '$locationProvider', nastavitev]);
    
})();