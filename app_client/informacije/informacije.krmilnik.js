(function() {
  /* global angular */
  
  function informacijeCtrl() {
    var vm = this;
    
    vm.glavaStrani = {
      title: 'Informacije'
    };
  };
  
  angular
    .module('spletna_trgovina')
    .controller('informacijeCtrl', informacijeCtrl);
})();