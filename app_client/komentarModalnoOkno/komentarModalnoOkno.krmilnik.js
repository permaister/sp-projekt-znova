(function() {
  /* global angular */
  
  function komentarModalnoOkno($uibModalInstance, trgovinaPodatki, podrobnostiIzdelka) {
    var vm = this;
    
    vm.podrobnostiIzdelka = podrobnostiIzdelka;
    
    vm.modalnoOkno = {
      zapri: function(odgovor){
        $uibModalInstance.close(odgovor);
      },
      preklici: function() {
        $uibModalInstance.close();
      }
    };
    
    vm.dodajKomentar = function(idIzdelka, podatkiObrazca){
      trgovinaPodatki.dodajKomentar(idIzdelka, {
        ocena: podatkiObrazca.ocena,
        komentar: podatkiObrazca.komentar
      }).then(
        function sucess(odgovor){
          vm.modalnoOkno.zapri(odgovor.data);
        },
        function error(odgovor){
          vm.napakaNaObrazcu = "Napaka pri shranjevanju komentarja, poskusite znova!";
        }
      );
      return false;
    }
    
    vm.posiljanjePodatkov = function(){
      vm.napakaNaObrazcu = "";
      if (!vm.podatkiObrazca.ocena || !vm.podatkiObrazca.komentar) {
        vm.napakaNaObrazcu = "Prosim, izpolnite vsa vnosna polja!";
        return false;
      }
      else{
        vm.dodajKomentar(vm.podrobnostiIzdelka.idIzdelka, vm.podatkiObrazca);
      }
    }
  };
  
  komentarModalnoOkno.$inject = ['$uibModalInstance', 'trgovinaPodatki', 'podrobnostiIzdelka'];
  
  angular
    .module('spletna_trgovina')
    .controller('komentarModalnoOkno', komentarModalnoOkno);
})();