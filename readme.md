Spletna trgovina je namenjena za knjige in video gradivo.

Povezava na Heroku: https://spletnik.herokuapp.com/

Vzpostavitev na lokalnem okolju:

cd spletna_trgovina

npm install

npm install -g express-generator

npm install --save mongoose

heroku config:set MLAB_URI=mongodb://user:pass@ds159696.mlab.com:59696/spletna_trgovina

mongodump -h localhost:27017 -d spletna_trgovina -o ~/workspace/spletna_trgovina/mongodb/dump

mongorestore -h ds159696.mlab.com:59696 -d spletna_trgovina -u user -p pass ~/workspace/spletna_trgovina/mongodb/dump/spletna_trgovina

mongo ds159696.mlab.com:59696/spletna_trgovina -u user -p pass