/* global angular */
angular.module('spletna_trgovina', []);

var trgovinaPodatki = function($http) {
      return $http.get('/api/izdelki');
};

var seznamIzdelkovCtrl = function($scope, trgovinaPodatki){
    
    $scope.sporocilo = "Pridobivam izdelke iz kataloga.";
    
    $scope.pridobiPodatke = function(){
        trgovinaPodatki.then(
            function success(odgovor){
                $scope.data = {izdelki : odgovor.data};
            },
            function error(odgovor){
                console.log(odgovor.e);
            }
        );
    }
    
    $scope.prikaziNapako = function(napaka) {
      $scope.$apply(function() {
        $scope.sporocilo = napaka.message;
      });
    };
    
    trgovinaPodatki();
};



var prikaziOceno = function() {
    return {
        scope: {
            trenutnaOcena: '=ocena'
        },
        templateUrl: "/angular/ocena-zvezdice.html"
    };
};

angular
    .module('spletna_trgovina')
    .controller('seznamIzdelkovCtrl', seznamIzdelkovCtrl)
    .directive('prikaziOceno', prikaziOceno)
    .service('trgovinaPodatki', trgovinaPodatki);
    
    