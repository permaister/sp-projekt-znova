var express = require('express');
var router = express.Router();
var jwt = require('express-jwt');
var avtentikacija = jwt({
    secret: process.env.JWT_GESLO,
    userProperty: 'payload'
});
var ctrlIzdelki= require('../controllers/izdelki');
var ctrlKomentarji = require('../controllers/komentarji');
var ctrlAvtentikacija = require('../controllers/avtentikacija');
var ctrlKosarica = require('../controllers/kosarica');

// Usmerjanje glede izdelkov
router.get('/izdelki', ctrlIzdelki.izdelkiSeznam);
router.post('/izdelki', ctrlIzdelki.izdelkiUstvari);
router.get('/izdelki/:idIzdelka', ctrlIzdelki.izdelkiPreglej);
router.put('/izdelki/:idIzdelka', ctrlIzdelki.izdelkiPosodobi);
router.delete('/izdelki/:idIzdelka', ctrlIzdelki.izdelkiIzbrisi);

// Komentarji
router.post('/izdelki/:idIzdelka/komentarji', avtentikacija, ctrlKomentarji.komentarjiUstvari);
router.get('/izdelki/:idIzdelka/komentarji/:idKomentarja', avtentikacija, ctrlKomentarji.komentarjiPreberi);
router.delete('/izdelki/:idIzdelka/komentarji/:idKomentarja', avtentikacija, ctrlKomentarji.komentarjiIzbrisi);

//Avtentikacija
router.post('/registracija', ctrlAvtentikacija.registracija);
router.post('/prijava', ctrlAvtentikacija.prijava);

//Kosarica
router.get('/kosarica/:idUporabnika', avtentikacija, ctrlKosarica.prikaziKosarico);
router.post('/kosarica/:idUporabnika', avtentikacija,  ctrlKosarica.dodajArtikelVKosarico);
router.post('/kos/:idUporabnika', avtentikacija, ctrlKosarica.odstraniArtikel);
router.post('/zak/:idUporabnika', ctrlKosarica.zakljuciNakup);

module.exports = router;