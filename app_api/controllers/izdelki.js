var mongoose = require('mongoose');
var Izdelek = mongoose.model('Izdelek');

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
  odgovor.status(status);
  odgovor.json(vsebina);
};

module.exports.izdelkiSeznam = function(zahteva, odgovor) {
    Izdelek
        .find()
        .exec(function(napaka, izdelek){
            if(napaka){
                vrniJsonOdgovor(odgovor, 404, napaka);
                return;
            }else if(!izdelek){
                vrniJsonOdgovor(odgovor, 404, {"sporocilo": "Ne najdem nobenih izdelkov"});
                return;
            }
            vrniJsonOdgovor(odgovor, 200, izdelek); 
        });
};

module.exports.izdelkiPreglej = function(zahteva, odgovor) {
    if (zahteva.params && zahteva.params.idIzdelka) {
        Izdelek
            .findById(zahteva.params.idIzdelka)
            .exec(function(napaka, izdelek){
                if(!izdelek){
                    vrniJsonOdgovor(odgovor, 404, {"sporocilo":"Ne najdem izdelka z enolicnim identifikattorjem, ki ste ga podali"});
                    return;
                }else if(napaka){
                    vrniJsonOdgovor(odgovor, 404, napaka);
                    return;
                }
                vrniJsonOdgovor(odgovor, 200, izdelek); 
            });
    } else{
        vrniJsonOdgovor(odgovor, 404, {"sporocilo":"Manjka enolicni identifikator izdelka" });
    }
};

module.exports.izdelkiUstvari = function(zahteva, odgovor) {
    Izdelek.create({
        naziv: zahteva.body.naziv,
        avtor: zahteva.body.avtor,
        kategorija: zahteva.body.kategorija,
        zvrst: zahteva.body.zvrst,
        cena: parseFloat(zahteva.body.cena),
        opis: zahteva.body.opis,
        kljucne_besede: zahteva.body.kljucne_besede.split(),
        slika: zahteva.body.slika,
        izpostavi: zahteva.body.izpostavi
    }, function(napaka, izdelek){
        if(napaka){
            vrniJsonOdgovor(odgovor, 400, napaka);
        } else {
            vrniJsonOdgovor(odgovor, 201, izdelek);
        }
    }
    );
};

module.exports.izdelkiPosodobi = function(zahteva, odgovor) {
  if(!zahteva.params.idIzdelka){
      vrniJsonOdgovor(odgovor, 404, {"sporocilo": "Ne najdem izdelka. Id izdelka je obvezen parameter."});
      return;
  }
  Izdelek
    .findById(zahteva.params.idIzdelka)
    .select('-komentarji -ocena')
    .exec(
        function(napaka, izdelek){
            if(!izdelek){
                vrniJsonOdgovor(odgovor, 404, {"sporocilo": "Ne najdem izdelka"});
                return;
            }else if(napaka){
                vrniJsonOdgovor(odgovor, 400, napaka);
                return;
            }
            izdelek.naziv = zahteva.body.naziv;
            izdelek.avtor = zahteva.body.avtor;
            izdelek.kategorija = zahteva.body.kategorija;
            izdelek.zvrst = zahteva.body.zvrst;
            izdelek.cena = parseFloat(zahteva.body.cena);
            izdelek.opis = zahteva.body.opis;
            izdelek.kljucne_besede = zahteva.body.kljucne_besede.split();
            izdelek.slika = zahteva.body.slika;
            izdelek.izpostavi = zahteva.body.izpostavi;
            
            izdelek.save(function(napaka, izdelek){
                if(napaka){
                    vrniJsonOdgovor(odgovor, 404, napaka);
                }else {
                    vrniJsonOdgovor(odgovor, 201, izdelek);
                }
            });
        });
  
};

module.exports.izdelkiIzbrisi = function(zahteva, odgovor) {
  var idIzdelka = zahteva.params.idIzdelka;
  if(idIzdelka){
      Izdelek
        .findByIdAndRemove(idIzdelka)
        .exec(function(napaka, izdelek){
            if(napaka){
                vrniJsonOdgovor(odgovor, 400, napaka);
            } else {
                vrniJsonOdgovor(odgovor, 204, null);
            }
        });
  } else {
      vrniJsonOdgovor(odgovor, 404, {"sporocilo": "Ne najdem izdelka."});
  }
};