var mongoose = require('mongoose');
var Izdelek = mongoose.model('Izdelek');
var Uporabnik = mongoose.model('Uporabnik');

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
  odgovor.status(status);
  odgovor.json(vsebina);
};


//Branje komentarja //
module.exports.komentarjiPreberi = function(zahteva, odgovor) {
  if(zahteva.params && zahteva.params.idIzdelka && zahteva.params.idKomentarja) {
     Izdelek
        .findById(zahteva.params.idIzdelka)
        .select('naziv komentarji')
        .exec(
            function(napaka, izdelek){
                var rezultat, komentar;
                if(!izdelek){
                    vrniJsonOdgovor(odgovor, 404, {"sporocilo":"Ne najdem izdelka s tem ID."});
                    return;
                }else if(napaka){
                    vrniJsonOdgovor(odgovor, 404, napaka);
                    return;
                }
                if(izdelek.komentarji && izdelek.komentarji.length > 0){
                    komentar = izdelek.komentarji.id(zahteva.params.idKomentarja);
                    if(!komentar){
                        vrniJsonOdgovor(odgovor, 404, {"sporocilo": "Ne najdem komentarja z podanim ID komentarja."});
                    } else {
                        rezultat = {
                            izdelek: {naziv: izdelek.naziv, id: zahteva.params.idIzdelka},
                            komentar: komentar
                        };
                        vrniJsonOdgovor(odgovor, 200, rezultat);
                    }
                } else {
                    vrniJsonOdgovor(odgovor, 404, {"sporocilo": "Ne najdem nobenega komentarja za izbrani izdelek"});
                }
            }
        );
  } else {
      vrniJsonOdgovor(odgovor, 404, {"sporocilo":"Zahtevana sta idIzdelka in idKomentarja"});
  }
};


//  Ustvarjanje komentarja //
module.exports.komentarjiUstvari = function(zahteva, odgovor) {
    vrniAvtorja(zahteva, odgovor, function(zahteva, odgovor, imeUporabnika) {
      var idIzdelka = zahteva.params.idIzdelka;
      if(idIzdelka){
          Izdelek
            .findById(idIzdelka)
            .select('komentarji')
            .exec(function(napaka, izdelek){
                if(napaka){
                    vrniJsonOdgovor(odgovor, 400, napaka);
                } else {
                    dodajKomentar(zahteva, odgovor, izdelek, imeUporabnika);
                }
            });
      } else {
          vrniJsonOdgovor(odgovor, 404, {"sporocilo": "Ne najdem izdelka. ID  je obvezen parameter."});
      }
    });
};

var dodajKomentar = function(zahteva, odgovor, izdelek, avtor){
    if(!izdelek){
        vrniJsonOdgovor(odgovor, 404, {"sporocilo": "Ne najdem izdelka."});
    } else {
        izdelek.komentarji.push({
            avtor: avtor,
            ocena: zahteva.body.ocena,
            besediloKomentarja: zahteva.body.komentar
        });
        izdelek.save(function(napaka, izdelek){
            var dodaniKomentar;
            if (napaka){
                console.log(napaka);
                vrniJsonOdgovor(odgovor, 400, napaka);
            } else {
                posodobiPovprecnoOceno(izdelek._id);
                dodaniKomentar = izdelek.komentarji[izdelek.komentarji.length -1];
                vrniJsonOdgovor(odgovor, 201, dodaniKomentar);
            }
        });
    }
};

var posodobiPovprecnoOceno = function(idIzdelka){
    Izdelek.findById(idIzdelka)
        .select('ocena, komentarji')
        .exec(function(napaka, izdelek){
            if(!napaka){
                izracunajPovprecnoOceno(izdelek);
            }
        });
}; 


var izracunajPovprecnoOceno = function(izdelek){
    var stevec, steviloKomentarjev, povprecnaOcena, skupnaOcena;
    if(izdelek.komentarji && izdelek.komentarji.length >0){
        steviloKomentarjev = izdelek.komentarji.length;
        skupnaOcena = 0;
        for(stevec=0; stevec<steviloKomentarjev; stevec++){
            skupnaOcena += izdelek.komentarji[stevec].ocena;
        }
        povprecnaOcena = parseInt(skupnaOcena / steviloKomentarjev, 10);
        izdelek.ocena = povprecnaOcena;
        izdelek.save(function(napaka){
            if(napaka){
                console.log("Napaka pri racunanju ocene: "+napaka);
            }
            else{
                console.log("Povprecna ocena je posodobljena na :"+povprecnaOcena);
            }
        });
    }
};

module.exports.komentarjiIzbrisi = function(zahteva, odgovor) {
    if(!zahteva.params.idIzdelka || !zahteva.params.idKomentarja){
        vrniJsonOdgovor(odgovor, 404, {"sporocilo" : "Ne najdem komentarja ali izdelka."});
        return;
    }
    Izdelek.findById(zahteva.params.idIzdelka)
        .exec(function(napaka, izdelek){
            if(!izdelek){
                vrniJsonOdgovor(odgovor, 404, {"sporocilo" : "Izdelka ne morem najti"});
                return;
            }else if (napaka){
                vrniJsonOdgovor(odgovor, 400, napaka);
                return;
            }
            if(izdelek.komentarji && izdelek.komentarji.length > 0){
                if(!izdelek.komentarji.id(zahteva.params.idKomentarja)){
                    vrniJsonOdgovor(odgovor, 404, {"sporocilo" : "Komentarja ne najdem."});
                } else {
                    izdelek.komentarji.id(zahteva.params.idKomentarja).remove();
                    izdelek.save(function(napaka){
                        if(napaka){
                            vrniJsonOdgovor(odgovor, 400, napaka);
                        } else {
                            posodobiPovprecnoOceno(izdelek._id);
                            vrniJsonOdgovor(odgovor, 204, null);
                        }
                    });
                }
            } else {
                vrniJsonOdgovor(odgovor, 404, {"sporocilo": "Ni komentarja za brisanje."});
            }
        });
};


var vrniAvtorja = function(zahteva, odgovor, povratniKlic) {
  if (zahteva.payload && zahteva.payload.elektronskiNaslov) {
    Uporabnik
      .findOne({ elektronskiNaslov: zahteva.payload.elektronskiNaslov })
      .exec(function(napaka, uporabnik) {
        if (!uporabnik) {
          vrniJsonOdgovor(odgovor, 404, {
            "sporočilo": "Ne najdem uporabnika."
          });
          return;
        } else if (napaka) {
          console.log(napaka);
          vrniJsonOdgovor(odgovor, 404, napaka);
          return;
        }
        povratniKlic(zahteva, odgovor, uporabnik.ime);
      });
  } else {
    vrniJsonOdgovor(odgovor, 404, {
      "sporočilo": "Ne najdem uporabnika"
    });
    return;
  }
};