var mongoose = require('mongoose');
var Kosarica = mongoose.model('Kosarica');
var Uporabnik = mongoose.model('Uporabnik');
var Izdelek = mongoose.model('Izdelek');

var kos = [];

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
  //console.log("Vračam JSOn odgovor. Vsebina: "+vsebina+" Status: "+status);
  odgovor.status(status);
  odgovor.json(vsebina);
};


module.exports.prikaziKosarico = function(zahteva, odgovor) {
    console.log("V metodi prikazi kosarico.");
    if(zahteva.params && zahteva.params.idUporabnika){
      //console.log("Imamo parameter kupca.");
      Kosarica
        .findOne({kupec: zahteva.params.idUporabnika, jeZakljucena: false})
        .exec(function(napaka, kosarica){
          //console.log("Kosarica: "+kosarica);
          if(!kosarica){
            //console.log("Ni nobene odprte kosarice");
            //console.log("idKupca: "+zahteva.params.idUporabnika);
            Kosarica.create({
              kupec: zahteva.params.idUporabnika,
              jeZakljucena: false,
              idUporabnika : zahteva.params.idUporabnika
            }, function(napaka, kosarica){
                console.log("Ustvarjamo kosarico.");
                  if(napaka){
                      //console.log("Prislo je do napake");
                      vrniJsonOdgovor(odgovor, 400, napaka);
                  } else {
                      //console.log("Zdaj smo ustvarili kosarico.");
                      console.log(kosarica);
                      vrniJsonOdgovor(odgovor, 201, kosarica);
                  }
                  return;
                });
          }else if(napaka){
            vrniJsonOdgovor(odgovor, 404, napaka);
            return;
          }else{
          //console.log("KOsarica najdena.");
          //console.log("Kosarica: "+kosarica);
          if(kosarica.artikli.length > 0){
            for(var i = 0; i< kosarica.artikli.length; i++){
              console.log(kosarica.artikli[i].artikel);
              Izdelek.findById(kosarica.artikli[i].artikel).exec(function(napaka, izdelek){
              if(napaka){
                  //console.log("Napaka v iskanju izdelkov.");
                  console.log(napaka);
                  return;
              }else if(!izdelek){
                  //console.log("Ni izdelkov!")
                  return;
              }
                //console.log(izdelek.naziv);
                kos.push(JSON.parse(JSON.stringify(izdelek)));
              });
            }
          }
          //console.log("Da vidimo, kaj skriva kos:");
          //console.log(kos.length);
          vrniJsonOdgovor(odgovor, 200, {
            kosarica: kosarica,
            artikli: JSON.parse(JSON.stringify(kos))
          }); 
          kos = [];
          }
        });
    }
    else{
      vrniJsonOdgovor(odgovor, 404, {"sporocilo":"Zahtevan je idKupca"});
    }
};

module.exports.dodajArtikelVKosarico = function(zahteva, odgovor){
  console.log("V metodi dodaj artikel v kosarico.");
    if(zahteva.params && zahteva.params.idUporabnika){
      //console.log("Imamo parameter kupca.");
      Kosarica
        .findOne({kupec: zahteva.params.idUporabnika, jeZakljucena: false})
        .exec(function(napaka, kosarica){
          if(!kosarica){
            //console.log("Ni nobene odprte kosarice");
          }else if(napaka){
            vrniJsonOdgovor(odgovor, 404, napaka);
            return;
          }
          //console.log("KOsarica najdena.");
          kosarica.artikli.push({
            artikel : zahteva.body.idIzdelka,
            kolicina : zahteva.body.kolicina
          });
          kosarica.save(function(napaka, kosarica){
            var dodaniArtikel;
            if(napaka){
              console.log(napaka);
              vrniJsonOdgovor(odgovor, 400, napaka);
            }else{
              dodaniArtikel = kosarica.artikli[kosarica.artikli.length - 1];
              //console.log("ID kosarice: "+kosarica._id);
              vrniJsonOdgovor(odgovor, 201, dodaniArtikel);
            }
          })
        });
    }
    else{
      vrniJsonOdgovor(odgovor, 404, {"sporocilo":"Zahtevan je idKupca"});
    }
};

module.exports.odstraniArtikel = function(zahteva, odgovor) {
  console.log("Klical sem odstrani v apiju!!!!");
  if(!zahteva.params.idUporabnika || !zahteva.body.idIzdelka){
      vrniJsonOdgovor(odgovor, 404, {"sporocilo" : "Ne najdem kosarice ali artikla."});
      return;
  }
  Kosarica
    .findOne({kupec: zahteva.params.idUporabnika, jeZakljucena: false})
    .exec(function(napaka, kosarica){
      if(napaka){
        vrniJsonOdgovor(odgovor, 404, napaka);
        return;
      }
      //console.log("Artikli v kosarici: "+kosarica.artikli);
      //console.log("Odstranjujem artikel: "+zahteva.body.idIzdelka);
      var id;
      for(var i = 0; i< kosarica.artikli.length; i++){
        if(kosarica.artikli[i].artikel == zahteva.body.idIzdelka){
          console.log(kosarica.artikli[i])
          id = kosarica.artikli[i]._id;
        }
      }
      console.log("Tole je ID artikla: "+id);
      kosarica.artikli.id(id).remove();
      //kosarica.artikli.pull({artikel : zahteva.body.idIzdelka});
      //kosarica.artikli.remove({artikel: zahteva.body.idIzdelka});
      console.log("Artikli v kosarici po brisanju: "+kosarica.artikli);
      kosarica.save(function(napaka){
        if(napaka){
            vrniJsonOdgovor(odgovor, 400, napaka);
        } else {
            vrniJsonOdgovor(odgovor, 204, null);
        }
      });
    });
}; 


module.exports.zakljuciNakup = function(zahteva, odgovor){
  console.log("Zakljucujem nakup.")
  if(zahteva.params && zahteva.params.idUporabnika){
    Kosarica
      .findOne({kupec: zahteva.params.idUporabnika, jeZakljucena: false})
      .exec(function(napaka, kosarica){
        if(!kosarica){
                vrniJsonOdgovor(odgovor, 404, {"sporocilo": "Ne najdem kosarice"});
                return;
            }else if(napaka){
                vrniJsonOdgovor(odgovor, 400, napaka);
                return;
            }
            kosarica.jeZakljucena = true;
            
            kosarica.save(function(napaka, kosarica){
                if(napaka){
                    vrniJsonOdgovor(odgovor, 404, napaka);
                }else {
                    vrniJsonOdgovor(odgovor, 201, kosarica);
                }
            });
      })
  }else{
    vrniJsonOdgovor(odgovor, 404, {"sporocilo":"Zahtevan je idKupca"});
  }
}
  
  
  
  
  
  
  
  
  
  
    
