var mongoose = require('mongoose');

var artikelKosariceShema = new mongoose.Schema({
  artikel: {type: mongoose.Schema.Types.ObjectId, ref: 'Izdelek'},
  kolicina: {type: Number, required: true, min: 0},
});



var kosaricaShema = new mongoose.Schema({
    kupec: {type: mongoose.Schema.Types.ObjectId, ref: 'Uporabnik'},
    jeZakljucena: Boolean,
    datumNastanka: {type: Date, "default": Date.now},
    artikli: [artikelKosariceShema],
    idUporabnika: String
});

mongoose.model('Kosarica', kosaricaShema, 'Kosarice');