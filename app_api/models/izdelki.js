var mongoose = require('mongoose');

var komentarjiShema = new mongoose.Schema({
  avtor: {type: String, required: true},
  ocena: {type: Number, required: true, min: 0, max: 5},
  besediloKomentarja: {type: String, required: true},
  datum: {type: Date, "default": Date.now}
});



var izdelkiShema = new mongoose.Schema({
    naziv: {type: String, required: true},
    avtor: String,
    kategorija: String,
    zvrst: String,
    cena: Number,
    ocena: {type: Number, "default": 0, min: 0, max: 5},
    opis: String,  
    kljucne_besede: [String],
    slika: String,
    izpostavi: Boolean, 
    datum_dodajanja: {type: Date, "default": Date.now},
    komentarji: [komentarjiShema]
});

mongoose.model('Izdelek', izdelkiShema, 'Izdelki');